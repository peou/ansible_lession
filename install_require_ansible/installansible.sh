#!/bin/bash

sudo apt install python3-pip -y
python3 -m pip -V
pip3 install ansible
ansible --version
python3 -m pip show ansible
